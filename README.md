<h1 align="center">Qdrant Java 客户端 </h3>

### 前言：
此项目源码来源为：https://github.com/metaloom/qdrant-java-client 作者：metaloom ，非本发布者

本项目在gitee中发布的原因是由于gitee中很少有 [Qdrant 矢量数据库](https://qdrant.tech/) Java客户端的开源项目，作者metaloom在github发布的Java版Qdrant客户端又只支持jdk17及以上
低版本的jdk无法使用，所以再次进行改造不做升级与优化，将http方式的客户端提取出来，单独作为一个模块，在此发布

by: huangzy

### 以下引用原文：

这个项目包含一个用于 [Qdrant 矢量数据库](https://qdrant.tech/). 的 java 客户端。客户端以阻塞或非阻塞方式支持 HTTP 传输。对于异步操作，`Future`可以使用基于 RxJava3 的 API。


## Maven
`该项目尚未上传到maven中央仓库,所以需要自行clone本项目本地编译`
```xml
<dependency>
	<groupId>io.metaloom.qdrant</groupId>
	<artifactId>qdrant-java-http-client</artifactId>
	<version>0.11.0-jdk8</version>
</dependency>
```

## 用法 - HTTP

```java
try (QDrantHttpClient client = QDrantHttpClient.builder()
		.setHostname("localhost")
		.setPort(port)
		.build()) {

	// Create a collection
	CollectionCreateRequest req = new CollectionCreateRequest();
	req.setVectors("colors", 4, Distance.EUCLID);
	client.createCollection("the-collection-name", req).sync();

	// Now add some points
	PointStruct p1 = PointStruct.of("colors", 0.42f, 0.33f, 42.15f, 68.72f)
		.setPayload("{\"name\": \"first\"}")
		.setId(1);
	PointStruct p2 = PointStruct.of("colors", 0.76f, 0.43f, 63.45f, 22.10f)
		.setPayload("{ \"color\": \"red\"}")
		.setId(2);
	PointStruct p3 = PointStruct.of("colors", 0.41f, 0.32f, 42.11f, 68.71f).setId(3);
	PointStruct p4 = PointStruct.of("colors", 0.12f, 0.23f, 12.46f, 47.17f).setId(4);

	PointsListUpsertRequest pointsRequest = new PointsListUpsertRequest();
	pointsRequest.setPoints(p1, p2, p3, p4);
	client.upsertPoints("the-collection-name", pointsRequest, false).async().blockingGet();

	// List the collections
	client.listCollections().async().blockingGet();

	// Count the points in the collection
	client.countPoints("the-collection-name", new PointCountRequest().setExact(true)).sync();
}
```